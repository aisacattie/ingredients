﻿:- module(
       recipe_conversions,
       [
	   text_to_translated_fragments/2
       ]
   ).

:- use_module(library(clpfd)).

:- use_module('dcg_utils.prolog').
:- use_module('tokenization.prolog').
:- use_module('fast_keyphrase.prolog').


% todo: implement some sanity checks (10000000000 ml ??)

% warning: before implementing stuff using macros, properly define all
% meta-predicates!!!
%
% a vubec... jak spravne implementovat meta-predikat jako makro???

%-----------------------

% todo: measure performance benefits of making the following predicates
% into macros like this:
% goal_expansion(
%    t_type(Type, In, Out),
%    (
%	tkn(Token, In, Out),
%	token_type(Token, Type)
%    )
%).

t_type(Type) -->
	tkn(Token),
	{
	    token_type(Token, Type)
	}.

a_orig(Text_atom) -->
	tkn(Token),
	{
	    token_atom(Token, Text_atom)
	}.

a_orig(Text_atom, Type) -->
	tkn(Token),
	{
	    token_atom_and_type(Token, Text_atom, Type)
	}.

a(Text_atom_lc) -->
	[_-Text_codes],
	{
	    atom_codes(Text_atom, Text_codes),
	    downcase_atom(Text_atom, Text_atom_lc)
	}.

a(Text_atom_lc, Type) -->
	[Type-Text_codes],
	{
	    atom_codes(Text_atom, Text_codes),
	    downcase_atom(Text_atom, Text_atom_lc)
	}.

c_orig(Text_codes) -->
	tkn(Token),
	{
	    token_codes(Token, Text_codes)
	}.

%-----------------------

ws --> t_type(ws).

try_ws --> try_phrase(ws).

dash_like('—', dash).
dash_like('–', dash).
dash_like('‒',dash).
dash_like('-', 'hyphen-minus').
dash_like('−', minus).
dash_like('‐', hyphen).
dash_like('‑', hyphen).

%------------------------

%numword_table(zero,0).
numword_table(one,1).
numword_table(two,2).
numword_table(three,3).
numword_table(four,4).
numword_table(five,5).
numword_table(six,6).
numword_table(seven,7).
numword_table(eight,8).
numword_table(nine,9).

numword_table(ten,10).
numword_table(eleven,11).
numword_table(twelve,12).

numword(X) -->
	a(Text_atom),
	{
	    numword_table(Text_atom, X)
	}.

simple_number_or_numword(X) -->
	(
	    simple_number(X);
	    numword(X)
	),
	!.

denominator_word(half, s(a), 2).
denominator_word(halves, p, 2).

denominator_word(third, s(a), 3).
denominator_word(thirds, p, 3).

denominator_word(quarter, s(a), 4).
denominator_word(quarters, p, 4).

denominator_word(fourth,s(a),4).
denominator_word(fourths, p, 4).

denominator_word(eighth, s(an), 8).
denominator_word(eighths, p, 8).

indefinite_article(a) --> a(a).
indefinite_article(an) --> a(an).

% pozor: tim, ze neprijimam napr two third, stane se, ze misto toho
% prijmu jen to third, coz je jeste vetsi chyba !!!!!!
% todo: udelat to benevolentnejsi a tyto anomalie eventuelne jen logovat

natlang_fraction(X) -->
	(
	    [], {Numerator = 1};
	    indefinite_article(IA), ws, {Numerator = 1};
	    simple_number_or_numword(Numerator), try_ws
	),
	a(Denominator_word),
	{
	    (
		Numerator =:= 1 ->
		Grammatical_number = s(IA);
		Grammatical_number = p
	    ),
	    denominator_word(Denominator_word,Grammatical_number,Denominator),
	    X is Numerator/Denominator
	},
	!.

general_fraction(X) -->
	(
	    fraction(X);
	    natlang_fraction(X)
	),
	!.

simple_number_or_numword_or_general_fraction(X) -->
	(
	    general_fraction(X);
	    simple_number_or_numword(X)
	),
	!.

plus_like_token(and).
plus_like_token(plus).
plus_like_token('+').

plus_like_phrase -->
	a(Plus),
	{
	    plus_like_token(Plus)
	}.

andous_number(X) -->
	simple_number_or_numword_or_general_fraction(Val1),
	try_ws,
	plus_like_phrase,
	try_ws,
	general_fraction(Val2),
	{
	    X is Val1 + Val2
	}.

simple_number(N) -->
	a_orig(Text, digits),
	{
	    atom_number(Text, N),
	    (
		\+sub_atom(Text, 0, _, _, '0');
		N = 0
	    ),
	    !
	}.

slash_like_symbol('/').
slash_like_symbol('⁄'). %U+2044
slash_like_symbol('∕'). %U+2215

simple_fraction(Value) -->
	simple_number(Num),
	a_orig(Slash),
	{
	    slash_like_symbol(Slash)
	},
	simple_number(Denom),
	{
	    Denom > 0,
	    Value is Num/Denom
	}.


unicode_fraction_table('½',1/2).
unicode_fraction_table('¼',1/4).
unicode_fraction_table('¾',3/4).
unicode_fraction_table('⅐',1/7).
unicode_fraction_table('⅑',1/9).
unicode_fraction_table('⅒',1/10).
unicode_fraction_table('⅓',1/3).
unicode_fraction_table('⅔',2/3).
unicode_fraction_table('⅕',1/5).
unicode_fraction_table('⅖',2/5).
unicode_fraction_table('⅗',3/5).
unicode_fraction_table('⅘',4/5).
unicode_fraction_table('⅙',1/6).
unicode_fraction_table('⅚',5/6).
unicode_fraction_table('⅛',1/8).
unicode_fraction_table('⅜',3/8).
unicode_fraction_table('⅝',5/8).
unicode_fraction_table('⅞',7/8).

%unicode_fraction_table('↉',0/3). :-)

unicode_fraction(Val) -->
	a_orig(A),
	{
	    unicode_fraction_table(A,F),
	    Val is F
	}.

fraction(Value) -->
	simple_fraction(Value);
	unicode_fraction(Value).

silly_number(Value) -->
	simple_number(Val_whole),
	try_ws,
	(
	    a_orig(X),
	    {
	        dash_like(X,_)
	    };
	    []
	),
	try_ws,
	fraction(Val_fraction),
	{
	    Val_fraction < Val_whole,
	    Value is Val_whole + Val_fraction
	}.

decimal(us,Value) -->
	simple_number(Whole_part),
	a_orig('.'),  % or c_orig(`.`) if implemented
	a_orig(Fractional_part_atom, digits),
	{
	    atom_number(Whole_part_atom, Whole_part),
	    atomic_list_concat(
		[Whole_part_atom,'.',Fractional_part_atom],
		Atom_all
	    ),
	    atom_number(Atom_all, Value)
	}.

%approximately_like_token()

general_number(Value) -->
	%approximately_like,
	%try_ws,
	(
	    andous_number(Value);
	    decimal(us,Value);
	    silly_number(Value);
	    general_fraction(Value);
	    simple_number(Value);
	    numword(Value);
	    indefinite_article(_),{Value = 1}
	),
	!.

apply_conversion(Factor, In, Out) :-
	number(Factor),
	Out is Factor * In.

apply_conversion(function(Func), In, Out) :-
	call(Func, In, Out).


canonize_quantity(
    Numeric_value*Unit,
    Canonized_numeric_value*Canonized_unit
) :-
	unit(Unit, Conv_spec, Canonized_unit),
	apply_conversion(Conv_spec, Numeric_value, Canonized_numeric_value).

% todo: implement a phrase that is defined by tokenization of its
% argument, optionally with whitespace replaced by try_ws

% todo: consider converting the teaspoons and tablespoons only if not by
% themselves, like in 1/2 cup and 2 Tbsp.

canonic_unit(ml, volume).
canonic_unit(g, mass).
canonic_unit(cm, length).
canonic_unit('°C', temperature).

fahrenheit_celsius(F, C):-
	C is (F - 32) / 1.8.

% todo: use compound terms to indicate variant, e.g.
% tablespoon(us) etc.
% everything is US for now

unit(gallon, 3785.41, ml).
unit(quart, 946.35, ml).
unit(pint, 473.18, ml).
unit(cup, 236.59, ml).
unit(tablespoon, 14.79, ml).
unit(teaspoon,  4.93, ml).

unit(floz, 29.57, ml).

unit(ounce, 28.35, g).
unit(pound, 453.59, g).
unit(g, 1.0, trivial(g)). % fixme: solve this in a less ugly way
unit(kg, 1000.0, trivial(g)).

unit('stick of butter', 113.4, g).

unit(inch, 2.54, cm).

unit('°F',function(fahrenheit_celsius), '°C').

%conversion([``,``], 0, ``).

fast_keyphrase_macro(
    read_unit_phrase,
    [
	gallon-[`gallon`,`gallons`,`gal`,`gals`],
	cup-[`cup`,`cups`],
	tablespoon-[`T.`,`T`,`Tb.`,`Tb`,`tb.`,`tb`,`Tbs.`,`Tbs`,`tbs.`,`tbs`,`tblsp.`,`tblsp`,`tblsps.`,`tblsps`,`tblspn.`,`tblspn`,`tblspns.`,`tblspns`,`Tbsp.`,`Tbsp`,`tbsp.`,`tbsp`,`Tbsps.`,`Tbsps`,`tbsps.`,`tbsps`,`tablespoon`,`tablespoons`],
	teaspoon-[`Tsp.`,`Tsp`,`tsp.`,`tsp`,`teaspoon`,`teaspoons`],
	ounce-[`oz.`,`oz`,`ounce`,`ounces`],
	pound-[`Lb.`,`Lb`,`lb.`,`lb`,`lbs.`,`Lbs.`,`lbs`,`Lbs`,`pound`,`pounds`],
	g-[`g`,`gram`,`grams`],
	kg-[`kg`],
	inch-[`inch`,`inches`], % chybi 'in'
	quart-[`quart`,`quarts`,`qt.`,`qt`,`Qt.`,`Qt`],
	pint-[`p.`,`p`,`pt.`,`pt`,`pint`,`pints`],
	floz-[`floz.`,`floz`,`fl.oz.`,`fl.oz`,`fl oz.`, `fl oz`, `fluid ounce`, `fluid ounces`, [102, 108, 46, 32, 111, 122, 46],  [102, 108, 46, 32, 111, 122] ], % `fl. oz.` confuses pcemacs, so codes instead
	'stick of butter'-[`stick of butter`,`sticks of butter`,`sticks of unsalted butter`,`stick of unsalted butter`],
	'°F'-[`°F`,`° F`,`F`, `ºF`,`º F`, `℉`, `Fahrenheit`, `Fahrenheits`, `fahrenheit`, `fahrenheits`, `degree Fahrenheit`, `degrees Fahrenheit`, `degree fahrenheit`, `degrees Fahrenheit`]
    ]
).

read_unit_phrase_tolerant(X) -->
	read_unit_phrase(X) *->
	[];
	t_type(letters),
	try_ws,
	read_unit_phrase(X).

precision_digits(2).

% todo: if there are no decimal places, do not print the dot

smart_format(In,`0`):-
	In =:= 0,
	!.

smart_format(In,Out):-
	In =\= 0,
	precision_digits(P),
	N_frac_digits is round(max(P-log10(abs(In)),0)),
	number_codes(N_frac_digits,NFD_text),
	append([`~`,NFD_text,`f`],Format),
	format(codes(Out),Format,[In]).

range_symbol -->
	(
	    a_orig(X),  % todo: rename dash_like to dash_like_atom and make dash_like into a phrase
	    {
		dash_like(X,_)
	    };
	    a_orig(X), a_orig(X),
	    {
		dash_like(X,_)
	    };
	    a(to)
	).

range(X0, X1) -->
	general_number(X0),
	try_ws,
	range_symbol,
	try_ws,
	general_number(X1),
	{
	    X0<X1
	}.

% todo: use try(Phrase) instead or something
of_a_like -->
	[],
	(
	    a(of),ws,a(a); % todo: use a convenience macro to specify fixed phrases !
	    a(of),ws,a(an);
	    a(a);
	    a(an);
	    a_orig(X),
	    {
	        dash_like(X,_)
	    }
	),
	!.

single_quantity_simple(Numeric_value*Unit) -->
	general_number(Numeric_value),
	try_ws,
	try_phrase(of_a_like),
	try_ws,
	read_unit_phrase_tolerant(Unit).

single_quantity_general_canonized(
    %Expr,
    Canonized_numeric_value*Canonized_unit
) -->
	single_quantity_simple(Q0),
	{
	    canonize_quantity(Q0, Can_val_0*Canonized_unit)
	},
	(
	    try_ws,
	    plus_like_phrase,
	    try_ws,
	    single_quantity_simple(Q1),
	    {
		canonize_quantity(Q1, Can_val_1*Canonized_unit),
		Canonized_numeric_value is Can_val_0 + Can_val_1
	        %, Expr = Q0+Q1
	    };
	    {
		Canonized_numeric_value = Can_val_0
		%, Expr = Q0
	    }
	),
	!.

% todo: consider checking that the upper range end is actually upper
ranged_quantity((N0-N1)*Unit) -->
	range(N0, N1),
	try_ws,
	try_phrase(of_a_like),
	try_ws,
	read_unit_phrase_tolerant(Unit).

ranged_quantity_canonized(
    (Can_val_0-Can_val_1)*Canonized_unit
) -->
	ranged_quantity((N0-N1)*Unit),
	{
	    canonize_quantity(N0*Unit, Can_val_0*Canonized_unit),
	    canonize_quantity(N1*Unit, Can_val_1*Canonized_unit)
	}.

quantity_general_canonized(
    Canonized_quantity
) -->
	ranged_quantity_canonized(Canonized_quantity),
	!.

quantity_general_canonized(
    Canonized_general_value*Canonized_unit
) -->
	single_quantity_general_canonized(
	    Canonized_value_0*Canonized_unit
	),
	(
	    try_ws,
	    range_symbol,
	    try_ws,
	    single_quantity_general_canonized(
		Canonized_value_1*Canonized_unit
	    ),
	    {
		Canonized_general_value = Canonized_value_0-Canonized_value_1
	    };
	    {
		Canonized_general_value = Canonized_value_0
	    }
	),
	!.

%canonized_information(qgc(Qgc)) -->
%	quantity_general_canonized(Qgc).

% pozor: 1 oz ( 1/8 cup ) ingrediences ... will try to work with the
% ingrediences !!!!!!!

:- meta_predicate parenthesized(//, ?, ?).
parenthesized(Phrase) -->
	a_orig('('),
	call_dcg(Phrase),
	a_orig(')').


token_not_left_paren --> % ugh
	a_orig(Atom),
	{
	    \+ Atom = '('
	}.

read_paren_content(Length_limit, Content) --> % a bit clumsy...
	[],
	{
	    Len #=< Length_limit
	},
	parenthesized(
	    capture_matched(
		repeat(token_not_left_paren,Len),
		Content
	    )
	),
	!. % or use once//1


% not allowed to skip a right paren !
% read_ingredience(blah) -->
%	[].


fast_keyphrase_macro(
    dumb_ingredience,
    [
	i(sugar, 0.81*(g/ml))-[`sugar`],
	i(flour, 0.54*(g/ml))-[`flour`, `all-purpose flour`, `all purpose flour`]
    ]
).

scale_val(Val, Factor, Scaled_val) :-
	(
	    number(Val),
	    Scaled_val is Val * Factor
	    ;
	    N0-N1 = Val,
	    Scaled_val_0 is N0 * Factor,
	    Scaled_val_1 is N1 * Factor,
	    Scaled_val = Scaled_val_0-Scaled_val_1
	),
	!.

portray_qgc(
    General_numeric_value * Canonized_unit,
    Codes
) :-
% just use the atom as output text
	atom_codes(Canonized_unit,CU_text),
	(
	    number(General_numeric_value),
	    smart_format(General_numeric_value,Numeric_value_text),
	    append([Numeric_value_text,` `,CU_text],Codes)
	    ;
	    N0-N1 = General_numeric_value,
	    smart_format(N0,N0_text),
	    smart_format(N1,N1_text),
	    append([N0_text,` to `,N1_text,` `,CU_text],Codes)
	),
	!.

% fixme: log the exception !
:- meta_predicate fail_on_exception_in_phrase(//, ?, ?).
fail_on_exception_in_phrase(Phrase, In, Out) :-
	catch(
	    call_dcg(Phrase, In, Out),
	    _,
	    false
	).


parens_content_maxlength(10).

parenthetical_weight_info -->
	parenthesized((
	    try_ws,
	    quantity_general_canonized(_ * Mass_unit),
	    try_ws
	)),
	{once(member(Mass_unit, [g, trivial(g)]))}.


tokens_skipped -->
	try_ws,
	\+ parenthetical_weight_info,
	{parens_content_maxlength(Pcm)},
	try_phrase(read_paren_content(Pcm,_)).

read_aux_information(
    Qg_canonized,
    aux_inf_raw(
	Tokens_skipped,
	Qg_aux_tokens,
	Qg_aux_canonized
    )
) -->
	{Qg_canonized = Volume_in_ml * ml},
	capture_matched(
	    tokens_skipped,
	    Tokens_skipped
	),
	capture_matched(
	    (
		try_ws,
		try_phrase((
		    a(of),ws,a(a);
		    a(of),ws,a(an);
		    a(of)
		)),
		try_ws,
		dumb_ingredience(i(_,Density_val*(g/ml))),
		{
		    scale_val(Volume_in_ml, Density_val, Mass_in_g),
		    Qg_aux_canonized = Mass_in_g * g
		}
	    ),
	    Qg_aux_tokens
	).

untokenize(Tokens, Codes):-
	call_dcg(
	    repeat_2(c_orig, Codess),
	    Tokens
	),
	append(Codess, Codes).

translate_one(
	_{
	    qg_orig_str:Qg_orig_str,
	    qgc_str:Qgc_str,
	    maybe_aux_inf:Maybe_aux_information
	}
) -->
	capture_matched(
	    quantity_general_canonized(Qgc),  % todo: decide how to handle multiple solutions
	    Qg_orig_tokens
	),
	{
	    \+ Qgc = _ * trivial(_),
	    untokenize(Qg_orig_tokens, Qg_orig_codes),
	    string_codes(Qg_orig_str, Qg_orig_codes),
	    portray_qgc(Qgc,Qgc_codes),
	    string_codes(Qgc_str, Qgc_codes)
	},
	(
	    read_aux_information(
		Qgc,
		aux_inf_raw(
		    Tokens_skipped,
		    Qg_aux_tokens,
		    Qg_aux_canonized
		)
	    ) ->
	    {
		Maybe_aux_information = [
		    _{
			fragments_skipped:Fragments_skipped,
			qg_aux_str:Qg_aux_str,
			qg_aux_canonized_str:Qg_aux_canonized_str
		    }
		],
		tokens_to_translated_fragments(Tokens_skipped, Fragments_skipped),
		untokenize(Qg_aux_tokens,Qg_aux_codes),
		string_codes(Qg_aux_str,Qg_aux_codes),
		portray_qgc(Qg_aux_canonized, Qg_aux_canonized_codes),
		string_codes(Qg_aux_canonized_str,Qg_aux_canonized_codes)
	    }
	    ;
	    {Maybe_aux_information = []}
	).

try_translate_one(F) -->
	fail_on_exception_in_phrase( % todo: do not swallow the exception!
	    translate_one(Translated)
	) ->
	{
	    F = _{type:"translated",value:Translated}
	}
	;
	c_orig(Orig),
	{
	    string_codes(Orig_str, Orig),
	    F = _{type:"orig",value:Orig_str}
	}.

tokens_to_translated_fragments(Tokens, []) :- at_end(Tokens).
tokens_to_translated_fragments(Tokens, [Fragment|Fragments_rest]) :-
	try_translate_one(Fragment, Tokens, Tokens_next),
	tokens_to_translated_fragments(Tokens_next, Fragments_rest).

text_to_translated_fragments(Input, Fragments) :-
	once((
	    tokenize(Input, Tokens),
	    tokens_to_translated_fragments(Tokens, Fragments)
	)).










