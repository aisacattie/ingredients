﻿:- module(recipe_conversions_benchmark, [benchmark/0]).

:- use_module('recipe-unit-conversion.prolog').
:- use_module('recipes.prolog').

benchmark :-
	once(recipe(R,_)),
	append([R,R,R,R,R,R,R,R,R,R,R,R,R,R,R,R,R,R,R,R,R,R,R,R],R2),
	time(
	    annotate(R2,_)
	).
