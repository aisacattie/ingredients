:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_cors)).
:- use_module(library(http/http_json)).
:- use_module(library(http/http_log)).
:- use_module(library(http/http_files)).

:- use_module(library(settings)).

:- use_module('recipe-unit-conversion.prolog').

%% todo: this seems to work well, but triggers a redirect loop if
%% there is no specialized root handler
%:- http_handler(
%       root(''),
%       http_redirect(see_other,root('')),
%       [prefix]
%   ).

:- http_handler(
       root(page),
       http_redirect(moved,root('')),
       []
   ).

:- http_handler(
       root('favicon.png'),
       http_reply_file('files/odmerka.png', []),
       []
   ).
:- http_handler(
       root('files/js/jquery-1.11.2.min.js'),
       http_reply_file('files/js/jquery-1.11.2.min.js',[]),
       []
   ).

:- http_handler(
       root(''),
       http_reply_file('files/page.html', []),
       []
   ).
:- http_handler(root(api), translate, []).


http:location(files, '/files', []).
:- http_handler(files(.), http_reply_from_files('files/files',[]), [prefix]).

server(Port) :-
        http_server(http_dispatch, [port(Port)]).

translate(Request) :-
	member(method(post), Request), !,
	cors_enable,
	http_read_json_dict(Request, Json_request),
	string_codes(Json_request.input_text,Input_text_codes),
	http_log("~s",Json_request.input_text), % todo: move to bottom
	text_to_translated_fragments(Input_text_codes,Fragments),
	reply_json_dict(_{result:Fragments}).
