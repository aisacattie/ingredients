﻿:- module(fast_keyphrase, [term_expansion/2]).

:- use_module('utils.prolog').
:- use_module('tokenization.prolog').

fast_keyphrase_helper_clauses(
    Helper_name,
    Tag_keyphrases_pairs,
    Helper_clauses
) :-
	findall(
	    Helper_clause,
	    (
		member(Tag-Keyphrases,Tag_keyphrases_pairs),
		member(Keyphrase,Keyphrases),
		tokenize(Keyphrase, KP_tokenized),
		tokens_as_list(
		    KP_tokenized,
		    [Keytoken_first | Keytokens_rest]
		),
		token_atom(Keytoken_first, Keytoken_first_atom),
		Helper_clause =.. [
		    Helper_name,
		    Keytoken_first_atom,
		    Keytokens_rest,
		    Tag
		]
	    ),
	    Helper_clauses
	).

fast_keyphrase_main_clause(
    Name,
    Helper_name,
    Phrase_clause
) :-
	% fixme: this works only if the dcg operates on lists!!! % todo: Make the helper clause return a phrase instead of Keytokens_rest
	unescape_term(
	    (
	        ''(Name, Tag_var) -->
		a_orig(Keytoken_first_atom_var),
		{
		    ''(
			Helper_name,
			Keytoken_first_atom_var,
			Keytokens_rest_var,
			Tag_var
		    )
		},
		Keytokens_rest_var
	    ),
	    Phrase_clause
	).

fast_keyphrase_clauses(
    Name, Tag_keyphrases_pairs,
    Clauses
) :-
	atom_concat(Name,'_fast_keyphrase_helper',Helper_name),
	fast_keyphrase_helper_clauses(
	    Helper_name, Tag_keyphrases_pairs,
	    Helper_clauses
	),
	fast_keyphrase_main_clause(
	    Name, Helper_name,
	    Phrase_clause
	),
	% fixme: find the reason why dcg expansion does not happen automatically if the output is a list !!!
	expand_term(Phrase_clause,Phrase_clause_expanded),
	append(
	    Helper_clauses,
	    [Phrase_clause_expanded],
	    Clauses
	).

term_expansion(
    fast_keyphrase_macro(Name, Tag_keyphrases_pairs),
    Clauses
) :-
	fast_keyphrase_clauses(
	    Name, Tag_keyphrases_pairs,
	    Clauses
	).




