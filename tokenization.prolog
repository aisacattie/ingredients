﻿:- module(
       tokenization,
       [
	   tokenize/2,
	   %tokenize_as_list/2,
	   at_end/1,
	   dcg_end/2,
	   call_dcg/2,
	   tkn//1,
	   tkn//0,
	   token_atom/2,
	   token_atom_and_type/3,
	   token_codes/2,
	   token_codes_and_type/3,
	   token_type/2,
	   tokens_as_list/2, % can we get rid of this?
	   capture_matched//2
       ]
   ).

:- use_module('dcg_utils.prolog').


% todo: consider using Prolog 7 strings instead of atoms in token
% matching or tokens themselves

our_char(letter,Letter) -->
	[Letter],
	{code_type(Letter,alpha)}.

our_char(digit,Digit) -->
	[Digit],
	{code_type(Digit,digit)}.

our_char(ws,WS) -->
	[WS],
	{member(WS,` \t\n`)}.

our_char(other,Val) -->
	\+our_char(letter,_),
	\+our_char(digit,_),
	\+our_char(ws,_),
	[Val].

token(letters-Letters) -->
	one_or_more_greedy_2(our_char(letter),Letters).

token(digits-Numbers) -->
	one_or_more_greedy_2(our_char(digit),Numbers).

token(ws-Vals) -->
	one_or_more_greedy_2(our_char(ws),Vals).

token(other-[Val]) -->
	our_char(other,Val).

tokens(Tokens) -->
	repeat_2(token,Tokens).

% consider this: during tokenization, store positions
% in the input text ! consider converting
% all letters to lower case during tokenization

% todo: consider making lc atoms already during tokenization
tokenize_as_list(Input,Tokens) :-
	once(
	    tokens(Tokens,Input,[])
	).

tokenize(Input, Tokens) :-
	tokenize_as_list(Input, Tokens).

%-----------------

% or the same for an atom
%tokenize_inplace_string(
%    Input_string,
%    Input_string-Positions_and_types
%). % Positions_and_types will be an "array" a(A,B,C,...)

% token_array_with_cursor(Token_array, Token_array-0).


% todo: measure performance benefits of making the following predicates
% into this macro:
%goal_expansion(
%    tkn(T,In,Out),
%    In = [T|Out]
%).

dcg_end(_, []).

at_end(X) :- dcg_end(X,X).

:- meta_predicate call_dcg(//, ?).
call_dcg(Phrase, Tokens) :-
	dcg_end(Tokens, Tokens_end),
	call_dcg(Phrase, Tokens, Tokens_end).


%take_interval(Tokens, Begin, End) :- % ugh, ugly?
%	append(Tokens,End,Begin).

%-----------------------

tkn(Token) --> [Token].

tkn --> [_].

%-----------------------

token_atom(_-Codes, Atom) :-
	atom_codes(Atom, Codes).

token_atom_and_type(Type-Codes, Atom, Type) :-
	atom_codes(Atom, Codes).

token_codes(_-Codes, Codes).

token_codes_and_type(Type-Codes, Codes, Type).

token_type(Type-_, Type).

%-----------------------

tokens_as_list(Token_list, Token_list).

:- meta_predicate capture_matched(//, ?, ?, ?).
capture_matched(Phrase,Tokens) -->
	capture_matched_interval_raw(Phrase, Input_raw-Output_raw),
	{
	    append(Tokens, Output_raw, Input_raw)
	}.







