﻿:- module(recipe_conversions_recipes, [recipe/2]).

recipe(
`1 1/4 cups all-purpose flour
 1/3 cup unsweetened cocoa powder
 1/2 teaspoon baking soda
 1 stick plus 3 tablespoons unsalted butter, at room temperature
 2/3 cup (packed) light brown sugar
 1/4 cup sugar
 1/2 teaspoon fleur de sel or 1/4 teaspoon fine sea salt
 1 teaspoon vanilla extract
 5 ounces bittersweet chocolate, chopped into chips, or a generous 3/4 cup store-bought mini chocolate chips`,
    _
).
