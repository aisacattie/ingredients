﻿:- module(utils, [unescape_term/2]).

unescape_term(Term_in, Term_out) :-
	once((
	    Term_in = quote(Term_out)
	    ;
	    compound(Term_in),
	    compound_name_arguments(
		Term_in,
		Name,
		Arguments
	    ),
	    once((
		Name = '',
		[New_name|New_args_escaped] = Arguments
		;
		New_name = Name,
		New_args_escaped = Arguments
	    )),
	    maplist(
		unescape_term,
		New_args_escaped,
		New_args
	    ),
	    compound_name_arguments(
		Term_out,
		New_name,
		New_args
	    )
	    ;
	    Term_in = Term_out
	)).


