﻿:- module(
       dcg_utils,
       [
	   try_phrase//1,
	   try_phrase_2//2,
	   repeat//2,
	   repeat_2//2,
	   repeat_2_greedy//2,
	   one_or_more_greedy_2//2,
	   capture//1,
	   capture_matched_interval_raw//2,
	   dcg_peek//1,
	   dcg_once//1
       ]
   ).

:- use_module(library(clpfd)).

%todo: think hard about steadfastness in this module
% esp. steadfastness with respect to the second DCG argument

%todo: consider implementing those as a macros

%todo: implement using dcg_once
:- meta_predicate try_phrase(//, ?, ?).
try_phrase(Phrase) -->
	call_dcg(Phrase) ->
	[];
	[].

%untested
%:- meta_predicate try_phrase_soft(//, ?, ?).
%try_phrase_soft(Phrase) -->
%	call_dcg(Phrase) *->
%	[];
%	[].

% note: untested!
:- meta_predicate try_phrase_2(3, ?, ?, ?).
try_phrase_2(Pred, Result_maybe) -->
	call(Pred, Result) ->
	{
	    Result_maybe = [Result]
	};
	{
	    Result_maybe = []
	}.

%--------------------------------------------

:- meta_predicate repeat(//, ?, ?, ?).
repeat(Phrase, Count) -->
	{
	    Count = 0
	};
	{
	    Count #> 0,
	    Count #= Count_next + 1
	},
	call_dcg(Phrase),
	repeat(Phrase, Count_next).

% todo: confirm this is tail recursive
:- meta_predicate repeat_2(3, ?, ?, ?).
repeat_2(E, Vals) -->
	{
	    Vals = []
	};
	call(E, Val),
	{
	    Vals = [Val|Vals_rest]
	},
	repeat_2(E, Vals_rest).

% todo: confirm this is tail recursive !
% beware: this is likely not steadfast w/ respect to the second DCG arg
repeat_2_greedy(E, Vals) -->
	call(E, Val) ->
	(
	    {
		Vals = [Val|Vals_rest]
	    },
	    repeat_2_greedy(E, Vals_rest)
	);
	{
	    Vals = []
	}.

:- meta_predicate one_or_more_greedy_2(3, ?, ?, ?).
one_or_more_greedy_2(E, Vals) -->
	{
	    Vals = [_|_]
	},
	repeat_2_greedy(E, Vals).

%--------------------------

capture(State,State,State).

:- meta_predicate capture_matched_interval_raw(//, ?, ?, ?).
capture_matched_interval_raw(Phrase,Input-Output) -->
	capture(Input),
	call_dcg(Phrase),
	capture(Output).

%dcg_inject_state(State, _, State).

%--------------------------

:- meta_predicate dcg_peek(//, ?, ?).
dcg_peek(Phrase, Input, Input) :-
	call_dcg(Phrase, Input, _).

%--------------------------
:- meta_predicate dcg_once(//, ?, ?).
dcg_once(Phrase) -->
	call_dcg(Phrase),
	!.






