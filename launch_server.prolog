﻿:- use_module(library(http/http_unix_daemon)).

% :- initialization set_setting(http:logfile,
% '/var/log/http_prolog.log'). % make sure it is accessible by
% www-data/_www

:- initialization set_setting(http:cors,[*]).

:- initialization http_daemon.

:- ['server.prolog'].
