#!/bin/bash
cd $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
#https://www.metalevel.at/letswicrypt/
swipl launch_server.prolog --https --user=www-data --keyfile=/etc/letsencrypt/live/converter.pavelbazant.com/privkey.pem --certfile=/etc/letsencrypt/live/converter.pavelbazant.com/fullchain.pem
